import NavItemsActionsTypes from "./nav-items.types";

export const toggleSidebar = () => ({
  type: NavItemsActionsTypes.TOGGLE_SIDEBAR,
});
