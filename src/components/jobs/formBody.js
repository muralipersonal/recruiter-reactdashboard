import React, { useState, useEffect } from "react";
import { get } from "../../utils/http";
import {
  Row,
  Col,
  Form,
  FormInput,
  FormSelect,
  FormGroup,
  FormTextarea,
  Button,
} from "shards-react";
import { usePosition } from "use-position";
import CreatableSelect from "react-select/creatable";
import { connect } from "react-redux";
import JobActionsTypes from "../../redux/job/job.types";
import { BASE_URL } from  "../../constants/API-routes";

const FormBody = ({ show, updateFileData, currentJob }) => {
  const [state, setState] = useState(currentJob);
  const [industries, setIndustries] = useState([]);
  const [subindustries, setSubindustries] = useState([]);
  const [skills, setSkills] = useState([]);
  const { latitude, longitude, error } = usePosition();

  const types = {
    jobTypes: ["full-time", "contract", "part-time", "internship", "freelance"],
    salaries: ["Per hour", "Per month", "Per year", "Depending Experience"],
  };

  const handleChange = (e) => {
    setState({
      ...state,
      [e.target.name]: e.target.value,
    });

    if (e.target.name === "industry") {
      const industry = industries.find(
        (industry) => industry.name === e.target.value
      );
      setSubindustries(industry.subIndustry);
    }
  };

  const handleSkillsChange = (skills) => {
    setState({
      ...state,
      skills,
    });
  };

  const handleSaveExit = async () => {
    const skills = [];
    if (state.skills != null) {
      state.skills.map((skill) => {
        skills.push(skill.value);
      });
    }

    updateFileData({
      ...state,
      skills,
    });

    show(false);
  };

  useEffect(() => {
    const fetchIndustries = async () => {
      try {
        const industries = await get(BASE_URL+"job/industry");
        setIndustries(industries);
        if (state.subIndustry && state.industry) {
          const industry = industries.find(
            (industry) => industry.name === state.industry
          );
          setSubindustries(industry.subIndustry);
        }
      } catch (err) {
        console.error(err.message);
      }
    };

    const fetchSkills = async () => {
      try {
        const dbSkills = await get(BASE_URL+"skill");
        let skillsOptions = [];
        dbSkills.forEach((skill) => {
          skillsOptions.push({
            value: skill.name,
            label: skill.name,
          });
        });
        setSkills(skillsOptions);
      } catch (err) {
        console.error(err.message);
      }
    };

    const fetchLocation = (lat, long) => {
      const KEY = process.env.REACT_APP_GOOGLE_MAPS_API_KEY;
      fetch(
        `https://maps.googleapis.com/maps/api/geocode/json?latlng=${lat},${long}&key=${KEY}`
      )
        .then((response) => response.json())
        .then((data) => {
          console.log(data);
        });
    };
    fetchIndustries();
    fetchSkills();

    if (latitude && longitude && !error) {
      fetchLocation(latitude, longitude);
    }
  }, [latitude, longitude]);

  return (
    <Col md="12">
      <Form>
        <Row form>
          <Col md="6" className="form-group">
            <label htmlFor="company">Company</label>
            <FormInput
              type="text"
              id="company"
              name="company"
              placeholder="Enter Company"
              value={state.company}
              onChange={(e) => handleChange(e)}
            />
          </Col>
          <Col md="6" className="form-group">
            <label htmlFor="jobTitle">Job Title</label>
            <FormInput
              type="text"
              id="jobTitle"
              name="jobTitle"
              placeholder="Enter Job Title"
              value={state.jobTitle}
              onChange={(e) => handleChange(e)}
            />
          </Col>
        </Row>
        <Row form>
          <Col md="6" className="form-group">
            <label htmlFor="industry">Industry</label>
            <FormSelect
              id="industry"
              name="industry"
              value={state.industry}
              onChange={(e) => handleChange(e)}
            >
              <option>Choose ...</option>
              {industries.map((industry, idx) => (
                <option key={idx}>{industry.name}</option>
              ))}
            </FormSelect>
          </Col>
          <Col md="6" className="form-group">
            <label htmlFor="subIndustry">Subindustry</label>
            <FormSelect
              id="subIndustry"
              name="subIndustry"
              value={state.subIndustry}
              onChange={(e) => handleChange(e)}
            >
              <option>Choose ...</option>
              {subindustries.map((subindustry, idx) => (
                <option key={idx}>{subindustry.name}</option>
              ))}
            </FormSelect>
          </Col>
        </Row>
        <Row form>
          <Col md="6" className="form-group">
            <label htmlFor="skills">Skills</label>
            <CreatableSelect
              id="skills"
              placeholder="Select Skills or enter a new"
              isMulti
              options={skills}
              value={state.skills}
              onChange={handleSkillsChange}
            />
          </Col>
          <Col md="6" className="form-group">
            <label htmlFor="type">Type</label>
            <FormSelect
              id="type"
              name="type"
              value={state.type}
              onChange={(e) => handleChange(e)}
            >
              <option>Choose ...</option>
              {types.jobTypes.map((type, idx) => (
                <option key={idx}>{type}</option>
              ))}
            </FormSelect>
          </Col>
        </Row>
        <Row form>
          <Col md="6" className="form-group">
            <label htmlFor="salary">Salary</label>
            <FormInput
              type="number"
              id="salary"
              name="salary"
              placeholder="Enter Salary"
              value={state.salary}
              onChange={(e) => handleChange(e)}
            />
          </Col>
          <Col md="6" className="form-group">
            <label htmlFor="salaryType">Salary Type</label>
            <FormSelect
              id="salaryType"
              name="salaryType"
              value={state.salaryType}
              onChange={(e) => handleChange(e)}
            >
              <option>Choose ...</option>
              {types.salaries.map((type, idx) => (
                <option key={idx}>{type}</option>
              ))}
            </FormSelect>
          </Col>
        </Row>
        <Row form>
          <Col md="6" className="form-group">
            <label htmlFor="contactName">Contact Name</label>
            <FormInput
              type="text"
              id="contactName"
              name="contactName"
              placeholder="Enter Contact Name"
              value={state.contactName}
              onChange={(e) => handleChange(e)}
            />
          </Col>
          <Col md="6" className="form-group">
            <label htmlFor="contact">Contact</label>
            <FormInput
              type="text"
              id="contact"
              name="contact"
              placeholder="Enter Contact Email"
              value={state.contact}
              onChange={(e) => handleChange(e)}
            />
          </Col>
        </Row>
        <Row form>
          <Col md="4" className="form-group">
            <label htmlFor="zip">Zip Code</label>
            <FormInput
              type="text"
              id="zip"
              name="zip"
              placeholder="Enter Zip Code"
              value={state.zip}
              onChange={(e) => handleChange(e)}
            />
          </Col>
          <Col md="4" className="form-group">
            <label htmlFor="country">Country</label>
            <FormInput
              type="text"
              id="country"
              name="country"
              placeholder="Enter Country Name"
              value={state.country}
              onChange={(e) => handleChange(e)}
            />
          </Col>
          <Col md="4" className="form-group">
            <label htmlFor="city">City</label>
            <FormInput
              type="text"
              id="city"
              name="city"
              placeholder="Enter City Name"
              value={state.city}
              onChange={(e) => handleChange(e)}
            />
          </Col>
        </Row>
        <FormGroup>
          <label htmlFor="description">Description</label>
          <FormTextarea
            className="form-textarea"
            id="description"
            name="description"
            placeholder="Enter a brief description"
            value={state.description}
            onChange={(e) => handleChange(e)}
          />
        </FormGroup>
      </Form>
      <div className="buttons-bar">
        <Button className="mr-2" onClick={handleSaveExit}>
          Update Job
        </Button>
        {/* <Button>Save and create Another job</Button> */}
      </div>
    </Col>
  );
};
const dispatchToProps = (dispatch) => ({
  updateFileData: (payload) => {
    dispatch({
      type: JobActionsTypes.UPDATE_FILE_DATA,
      payload,
    });
  },
});
export default connect(null, dispatchToProps)(FormBody);
